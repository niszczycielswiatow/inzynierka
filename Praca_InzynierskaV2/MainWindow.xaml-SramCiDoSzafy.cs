﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace Praca_InzynierskaV2
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly MySqlConnection connection = new MySqlConnection("server=127.0.0.1;uid=root;database=database;");
        readonly MenagmentSql menagmentSql = new MenagmentSql();
        readonly ClearTextBox clearTextBox = new ClearTextBox();
        readonly CheckIsEmpty checkIsEmpty = new CheckIsEmpty();

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new WindowViewModel(this);
            SearchData("");
        }
        
        private void Button_Magazyn(object sender, RoutedEventArgs e) => Zakladki.SelectedIndex = 1;

        private void Button_Dostawcy(object sender, RoutedEventArgs e)
        {
            Zakladki.SelectedIndex = 2;
            try
            {
                connection.Open();
                menagmentSql.dateInDataGridSupplier(connection);
                connection.Close();
            }
            catch (Exception blad)
            {
                MessageBox.Show(blad.Message);
            }
        }
        private void Button_Rozliczenia(object sender, RoutedEventArgs e) => Zakladki.SelectedIndex = 3;

        private void Button_Raporty(object sender, RoutedEventArgs e) => Zakladki.SelectedIndex = 4;

        private void DataGridSupplier_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            DataGrid gd = (DataGrid)sender;
            DataRowView row_selected = gd.SelectedItem as DataRowView;
            if (row_selected != null)
            {
                txtId.Text = row_selected[0].ToString();
                txtNazwa.Text = row_selected[1].ToString();
                txtNip.Text = row_selected[2].ToString();
                txtMiejscowosc.Text = row_selected[3].ToString();
                txtUlica.Text = row_selected[4].ToString();
                txtNr_domu.Text = row_selected[5].ToString();
                txtNr_tel.Text = row_selected[6].ToString();
                txtNr_konta.Text = row_selected[7].ToString();
            }
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {

            if (checkIsEmpty.CheckIsEmptyInSupplier())
            {
                MessageBox.Show("Uzupełnij wszystkie pola!");
            }
            else
            {
                string insertQuery = "INSERT INTO database.provider(name,nip,city,street,houseNumber,phoneNumber,bankAccount) VALUES ('" + txtNazwa.Text + "','" + txtNip.Text + "','" + txtMiejscowosc.Text + "','" + txtUlica.Text + "','" + txtNr_domu.Text + "','" + txtNr_tel.Text + "','" + txtNr_konta.Text + "')";
                connection.Open();
                MySqlCommand command = new MySqlCommand(insertQuery, connection);
                try
                {
                    if (command.ExecuteNonQuery() == 1)
                    {
                        menagmentSql.dateInDataGridSupplier(connection);
                        MessageBox.Show("Dodano");
                        clearTextBox.ClearTextBoxInSupplier();
                    }
                    else
                    {
                        MessageBox.Show("Nie dodano");
                    }
                }
                catch (Exception ex)
                {
                    if(ex.Message.StartsWith("Duplicate"))
                    {
                        MessageBox.Show("DOSTAWCA O TAKIM NUMERZE NIP JEST JUŻ W BAZIE!");
                    }
                    else
                        MessageBox.Show(ex.Message);
                }
                connection.Close();
            }
        }

        private void BtnEdytuj_Click(object sender, RoutedEventArgs e)
        {
            string updateQuery = "UPDATE database.provider SET name = '" + txtNazwa.Text + "', nip = '" + txtNip.Text + "', city = '" + txtMiejscowosc.Text + "', street = '" + txtUlica.Text + "', houseNumber = '" + txtNr_domu.Text + "', phoneNumber = '" + txtNr_tel.Text + "', bankAccount = '" + txtNr_konta.Text + "' WHERE id= '" + txtId.Text + "'";
            connection.Open();
            MySqlCommand command = new MySqlCommand(updateQuery, connection);

            try
            {
                if (command.ExecuteNonQuery() == 1)
                {
                    menagmentSql.dateInDataGridSupplier(connection);
                    MessageBox.Show("Edytowano");
                    clearTextBox.ClearTextBoxInSupplier();

                }
                else
                {
                    MessageBox.Show("Nie edytowano");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            connection.Close();
        }

        private void BtnUsun_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string deleteQuery = "DELETE FROM database.provider WHERE id=" + txtId.Text + "";
                connection.Open();
                MySqlCommand command = new MySqlCommand(deleteQuery, connection);

                if (command.ExecuteNonQuery() == 1)
                {
                    menagmentSql.dateInDataGridSupplier(connection);
                    MessageBox.Show("Usunieto");
                    clearTextBox.ClearTextBoxInSupplier();

                }
                else
                {
                    MessageBox.Show("Nie usunieto");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            connection.Close();

        }

        public void SearchData(string valueToSearch)
        {
            try
            {
                connection.Open();
                string query = "SELECT * FROM provider WHERE CONCAT(`name`,`nip`,`city`,`street`,`houseNumber`,`phoneNumber`,`bankAccount`) like '%" + valueToSearch + "%'";
                MySqlCommand command = new MySqlCommand(query, connection);
                MySqlDataAdapter AdapterSQL = new MySqlDataAdapter(command);
                DataSet dataSet = new DataSet();
                AdapterSQL.Fill(dataSet, "LoadDataBinding");
                DataGridSupplier.DataContext = dataSet;
            }
            catch (Exception blad)
            {
                MessageBox.Show(blad.Message);
            }
            connection.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string valueToSearch = txtSzukaj.Text.ToString();
            SearchData(valueToSearch);
        }

    }
}
